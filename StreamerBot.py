#!/bin/env python3
import requests
from sys import exit
from os import path
from pathlib import Path

class StreamerBot(object):
        client_id = ""
        client_secret = ""
        streamer = "Streamer"
        user_login = streamer.lower()
        api = "https://api.twitch.tv/helix"
        header_auth_key = "Authorization"
        header_auth_value = "Bearer " + client_secret
        header_client_key =  "Client-ID"
        header_client_value = client_id
        header_client = header_client_key + ': ' + header_client_value
        oauth2_url = "https://id.twitch.tv/oauth2"
        streamer_url = "https://www.twitch.tv/" + user_login
        #discord_notifications = "https://discord.com/api/webhooks"
        discord_hook = discord_notifications
        should_update_discord = False

        def get_broadcaster_id(self, token):
                api_users_url = self.api + "/users?login=" + self.user_login
                header_auth_value = "Bearer " + token
                headers_full = {self.header_auth_key: header_auth_value, self.header_client_key: self.header_client_value}
                r_users = requests.get(api_users_url, headers=headers_full).json()
                if len(r_users["data"]) > 0:
                        return r_users["data"][0]["id"]

        def get_token(self):
                token_url = self.oauth2_url + "/token?grant_type=client_credentials&client_secret=" + self.client_secret + "&client_id=" + self.client_id
                r = requests.post(token_url)
                return r.json()['access_token']

        def get_channel_status(self, token, broadcaster_id):
                api_channels_url = self.api + "/channels?broadcaster_id=" + broadcaster_id
                api_streams_url = self.api + "/streams?user_login=" + self.user_login
                header_auth_value = "Bearer " + token
                headers_full = {self.header_auth_key: header_auth_value, self.header_client_key: self.header_client_value}
                r_channels = requests.get(api_channels_url, headers=headers_full).json()
                r_streams = requests.get(api_streams_url, headers=headers_full).json()
                game_name = r_channels["data"][0]["game_name"]
                title = r_channels["data"][0]["title"]
                # twitch uses "type": "live" or ""
                if len(r_streams["data"]) > 0:
                        status_type = r_streams["data"][0]["type"]
                else: status_type = "offline"
                return game_name,title,status_type

        def revoke_token(self, token):
                revoke_url = self.oauth2_url + "/revoke?client_id=" + self.client_id + "&token=" + token
                r = requests.post(revoke_url)

        def update_discord_status(self, game_name, title):
                print('discord: ' + game_name)
                headers = {'Content-Type': 'application/json'}
                message = '@everyone ' + self.streamer + ' is now streaming ' + game_name
                payload = {'content': message}
                r_discord = requests.post(self.discord_hook, headers=headers, json=payload)
                print(r_discord)

        def compare_status(self, prev_status, game_name, title, status_type):
                # returns True if different False if same
                print("prev: " + prev_status)
                print("status: " + game_name + " " + title + " " + status_type)
                if game_name == "ASMR" and game_name not in prev_status and "live" in prev_status and status_type == "live":
                        return True
                print("status unchanged or offline")
                return False

if __name__ == '__main__':
        # status is game_name + " " + title
        status_file = path.expanduser('/home/m4tt4ew/gitlab/streamer_bot/status_file.txt')
        if path.isfile(status_file):
                with open(status_file, 'r') as previous_status:
                        prev_status = previous_status.readline()
                        token = StreamerBot().get_token()
                        broadcaster_id = StreamerBot().get_broadcaster_id(token)
                        game_name,title,status_type = StreamerBot().get_channel_status(token, broadcaster_id)
                        should_update_discord = StreamerBot().compare_status(prev_status, game_name, title, status_type)
                if should_update_discord:
                        StreamerBot().update_discord_status(game_name, title)
                with open(status_file, 'w') as previous_status:
                        previous_status.write(game_name + " " + status_type)
                StreamerBot().revoke_token(token)
        else:
                print('creating status file')
                Path(status_file).touch(mode=0o600, exist_ok=True)
                with open(status_file, 'w') as initial_status:
                        initial_status.write('Not Streaming')

        exit(0)

# TODO: add sleep/poll mechanism
# poll every 5 seconds from 3pm ET to 9pm ET
# poll every minute from 9pm ET to 3pm ET
